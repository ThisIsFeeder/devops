#!/bin/bash

RegistryImage=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${APP}:${TAG}
docker build -t ${RegistryImage} dotnet/
aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com
docker push ${RegistryImage}

aws cloudformation deploy \
    --stack-name "${StackName}" \
    --no-fail-on-empty-changeset \
    --template-file dotnet/cfn-app-${APP}.yaml \
    --parameter-overrides \
        Domain="${DOMAIN}" \
        RegistryImage="${RegistryImage}" \
        BasePath="${APP}" \
    --capabilities CAPABILITY_NAMED_IAM