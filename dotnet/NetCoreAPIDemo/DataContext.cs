
using Microsoft.EntityFrameworkCore;
using NetCoreAPIDemo.Models;

namespace NetCoreAPIDemo
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Book> Book { get; set; }
        public DbSet<Category> Category { get; set; }
    }
}