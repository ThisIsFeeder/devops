//@desc     Get all posts
//@route    GET /api/v1/post
//@access   Public
exports.getPosts = (req, res, next) => {
  res.status(200).json({
    success: true,
    message: "success",
    data: [
      {
        id: "07e1e5a3-56c4-46b0-939d-ccc9450a18c4",
        name: "STORY BOOK",
        description: "Describe loving in the world",
        authorId: "44342fd9-d220-4a6e-9a2e-e9262f0c2d49",
        categories: null,
      },
      {
        id: "0abb83b2-4394-4988-a330-0552b31f8366",
        name: "STORY BOOK",
        description: "Describe loving in the world",
        authorId: "44342fd9-d220-4a6e-9a2e-e9262f0c2d49",
        categories: null,
      },
    ],
  });
};

//@desc     Get post by id
//@route    GET /api/v1/post/:id
//@access   Public
exports.getPost = (req, res, next) => {
  res
    .status(200)
    .json({ success: true, message: `Get post by id ${req.params.id}` });
};

exports.getDefault = async (request, response) => {
  return response.send("Node Js")
}

//@desc     Create new post
//@route    POST /api/v1/post
//@access   Public
exports.createPost = (req, res, next) => {
  res.status(200).json({ success: true, message: "Create post" });
};

//@desc     Update post
//@route    PUT /api/v1/post/:id
//@access   Public
exports.updatePost = (req, res, next) => {
  res
    .status(200)
    .json({ success: true, message: `Update post id ${req.params.id}` });
};

//@desc     Delete post
//@route    DELETE /api/v1/:id
//@access   Public
exports.deletePost = (req, res, next) => {
  res
    .status(200)
    .json({ success: true, message: `Delete post id ${req.params.id}` });
};
