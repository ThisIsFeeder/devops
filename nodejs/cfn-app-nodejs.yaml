Description: baycon-apps-infrastructure
Parameters:
  # Ports
  HostPort:
    Description: Instance Port
    Default: 81
    Type: String

  ContainerPort:
    Description: Container Port micro services
    Default: 3000
    Type: String

  # ServiceName
  ServiceName:
    Description: Name of the service
    Default: nodejs-app
    Type: String

  RegistryImage:
    Description: Application Image
    Default: 386690131818.dkr.ecr.ap-southeast-1.amazonaws.com/nodejs
    Type: String

  Domain:
    Default: dariachi.info
    Type: String

  BasePath:
    Default: nodejs
    Type: String

Resources:
  # Security Group Ingress for nodejs
  SecurityGroupIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: allow connections for user target
      FromPort: !Ref HostPort
      ToPort: !Ref HostPort
      GroupId:
        Fn::ImportValue: 
          !Sub ${AWS::Region}-ServerSecurityGroupId
      IpProtocol: tcp
      CidrIp: '0.0.0.0/0'

  # NLB
  NetworkLoadBalancer:
    DependsOn:
      - ServiceDefinition
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      LoadBalancerAttributes:
        - Key: deletion_protection.enabled
          Value: 'false'
        - Key: load_balancing.cross_zone.enabled
          Value: 'true'
      Scheme: internet-facing
      Subnets:
        - Fn::ImportValue: !Sub ${AWS::Region}-PublicSubnet1
        - Fn::ImportValue: !Sub ${AWS::Region}-PublicSubnet2
        - Fn::ImportValue: !Sub ${AWS::Region}-PublicSubnet3
      Type: network
      Tags:
        - Key: Name
          Value: nodejs-nlb

  NetworkLoadBalancerTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckIntervalSeconds: 10 # NLB limits options to 10 and 30
      HealthCheckProtocol: HTTP
      HealthyThresholdCount: 3   # Healthy and unhealthy counts must be the same
      UnhealthyThresholdCount: 3 # Healthy and unhealthy counts must be the same
      Port: !Ref HostPort
      Protocol: TCP
      Tags:
        - Key: Name
          Value: core-nlb-targetgroup
      TargetGroupAttributes:
        - Key: deregistration_delay.timeout_seconds
          Value: '60'
      TargetType: instance
      VpcId:
        Fn::ImportValue: !Sub ${AWS::Region}-VpcId

  NetworkLoadBalancerListener:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      # Certificates: [CertificateArn: !Ref CertificateArnForRegion]
      DefaultActions:
        - TargetGroupArn: !Ref NetworkLoadBalancerTargetGroup
          Type: forward
          Order: 1
      LoadBalancerArn: !Ref NetworkLoadBalancer
      Port: !Ref HostPort
      Protocol: TCP

  ##
  # API GATEWAY
  # VPC Link is link from api gateway to NLB
  ##
  ApiGatewayVPCLink:
    Type: AWS::ApiGateway::VpcLink
    Properties:
      Description: ApiGateway Link to Network LoadBalancer in VPC
      Name: ApiGatewayVPCLink
      TargetArns:
        - !Ref NetworkLoadBalancer

  BasePathMapping:
    Type: AWS::ApiGateway::BasePathMapping
    Properties: 
      BasePath: !Ref BasePath
      DomainName: !Ref Domain
      RestApiId: !Ref ApiGatewayPolicy
      Stage: !Ref ApiStagePolicy

  ApiGatewayPolicy:
    Type: AWS::ApiGateway::RestApi
    Properties:
      Name: !Sub ${AWS::Region}-nodejs-gateway
      BinaryMediaTypes:
        - multipart/form-data

  ApiGatewayResource:
    Type: AWS::ApiGateway::Resource
    Properties:
      RestApiId: !Ref ApiGatewayPolicy
      ParentId: !GetAtt ApiGatewayPolicy.RootResourceId
      PathPart: '{proxy+}'

  ##
  # Using Policy Api Gateway
  ##
  ApiGatewayMethodPolicy:
    Type: AWS::ApiGateway::Method
    Properties:
      RestApiId: !Ref ApiGatewayPolicy
      ResourceId: !Ref ApiGatewayResource
      HttpMethod: ANY
      AuthorizationType: NONE
      RequestParameters:
        method.request.path.proxy: true
      Integration:
        Type: HTTP_PROXY
        CacheKeyParameters:
          - method.request.path.proxy
        RequestParameters:
          integration.request.path.proxy: method.request.path.proxy
        ConnectionId: ${stageVariables.VPCLINK}
        ConnectionType: VPC_LINK
        Uri: http://${stageVariables.VPCNLB}/{proxy}
        IntegrationHttpMethod: ANY
        PassthroughBehavior: WHEN_NO_MATCH
        IntegrationResponses:
          - StatusCode: '200'

  ApiStagePolicy:
    Type: AWS::ApiGateway::Stage
    Properties:
      MethodSettings:
        - LoggingLevel: INFO
          DataTraceEnabled: true
          HttpMethod: '*'
          ResourcePath: /*
          MetricsEnabled: true
      StageName: ApplicationStage
      RestApiId: !Ref ApiGatewayPolicy
      DeploymentId: !Ref ApiGatewayDeploymentPolicy
      Description: Stage ApiGateway
      Variables:
        VPCLINK: !Ref ApiGatewayVPCLink
        VPCNLB: !Sub ${NetworkLoadBalancer.DNSName}:${HostPort}

  ApiGatewayDeploymentPolicy:
    DependsOn:
      - ApiGatewayMethodPolicy
    Type: AWS::ApiGateway::Deployment
    Properties:
      RestApiId: !Ref ApiGatewayPolicy
      Description: Deploy ApiGateway

  ServiceDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: !Sub service-${ServiceName}
      ContainerDefinitions:
        - Name: !Sub  ${ServiceName}
          Essential: true
          Image: !Ref RegistryImage
          MemoryReservation: 128
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group:
                Fn::ImportValue:
                  !Sub ${AWS::Region}-LogGroupName
              awslogs-region: !Sub ${AWS::Region}
              awslogs-stream-prefix: !Sub ${AWS::Region}-service-${ServiceName}
          PortMappings:
            - ContainerPort: !Ref ContainerPort
              HostPort: !Ref HostPort
      ExecutionRoleArn: !Sub arn:aws:iam::${AWS::AccountId}:role/ecs
      NetworkMode: bridge

  Service:
    DependsOn:
      - NetworkLoadBalancerListener
    Type: AWS::ECS::Service
    Properties:
      Cluster:
        Fn::ImportValue:
          !Sub ${AWS::Region}-ServiceClusterName
      DeploymentConfiguration:
        MaximumPercent: 100
        MinimumHealthyPercent: 10
      HealthCheckGracePeriodSeconds: 60
      LoadBalancers:
        - ContainerName: !Sub ${ServiceName}
          ContainerPort: !Ref ContainerPort
          TargetGroupArn: !Ref NetworkLoadBalancerTargetGroup
      Role: ecs
      SchedulingStrategy: DAEMON
      Tags:
        - Key: Name
          Value: !Sub ${ServiceName}
      TaskDefinition: !Ref ServiceDefinition
