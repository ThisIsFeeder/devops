const express = require("express");

const {
  getPosts,
  getPost,
  createPost,
  updatePost,
  deletePost,
  getDefault
} = require("../controller/post");

const router = express.Router();

router.get("", getDefault);
router.route("/api/v1/post").get(getPosts).post(createPost);

router.route("/api/v1/post/:id").get(getPost).put(updatePost).delete(deletePost);

module.exports = router;
