const express = require("express");
const dotenv = require("dotenv");
const morgan = require("morgan");
//const logger = require('./middleware/logger');

//Load ENV
dotenv.config({ path: "./config/config.env" });

//Load Routes
const post = require("./route/post");

const app = express();

if (process.env.NODE_ENV === "development") {
  //Logger
  //app.use(logger); //Our customer logger middleware
  app.use(morgan("dev")); // Third party logger middleware
}

//Moute Router
app.use("", post);

const PORT = process.env.PORT || 3000;
const ENV = process.env.NODE_ENV || development;

app.listen(
  PORT,
  console.log(`Server is running in ${ENV} mode on port ${PORT}`)
);
