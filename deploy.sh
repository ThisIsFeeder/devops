S3_TEMPLATE=$(aws s3api list-buckets --query 'Buckets[?Name==`'${StackName}.${DOMAIN}'`].[Name]' --output text)

HostedZoneId=$(aws route53 list-hosted-zones \
    --query 'HostedZones[?Name==`'"${DOMAIN}"'.`].Id' \
    --output json | jq \
    --raw-output '.[0]' | sed 's/\/hostedzone\///')

CERTIFICATE_ARN=$(aws acm list-certificates --region us-east-1 --query 'CertificateSummaryList[?DomainName==`'"${DOMAIN}"'`].CertificateArn' --output text)

if [ "${S3_TEMPLATE}" != "${StackName}.${DOMAIN}" ]; then
    aws s3 mb s3://${StackName}.${DOMAIN} --region ${AWS_DEFAULT_REGION}
    aws s3api put-bucket-versioning --bucket ${StackName}.${DOMAIN} --versioning-configuration Status=Enabled
fi
  
aws s3 sync templates s3://${StackName}.${DOMAIN} --delete --quiet

aws cloudformation deploy \
    --stack-name "${StackName}" \
    --no-fail-on-empty-changeset \
    --template-file cfn-infrastructure.yaml \
    --parameter-overrides \
        Domain="${DOMAIN}" \
        CertificateArn="${CERTIFICATE_ARN}" \
        HostedZoneId="${HostedZoneId}"  \
        EnableNATMultiAZ="${EnableNATMultiAZ:-false}" \
        EnableServer="${EnableServer}" \
        EnableDB="${EnableDB}" \
        DatabaseInstance="${DatabaseInstance:-db.t3.micro}" \
        EC2InstanceType="${EC2InstanceType}" \
        EC2DesiredInstances="${EC2DesiredInstances}" \
        EC2MinimumInstances="${EC2MinimumInstances}" \
        EC2MaximumInstances="${EC2MaximumInstances}" \
    --capabilities CAPABILITY_NAMED_IAM