#!/bin/bash

RegistryImage=${REPOSITORY}/${APP}:${TAG}
docker build -t ${RegistryImage} java/
aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin ${REPOSITORY}
docker push ${RegistryImage}

aws cloudformation deploy \
    --stack-name "${StackName}" \
    --no-fail-on-empty-changeset \
    --template-file java/cfn-app-${APP}.yaml \
    --parameter-overrides \
        Domain="${DOMAIN}" \
        RegistryImage="${RegistryImage}" \
        BasePath="${APP}" \
    --capabilities CAPABILITY_NAMED_IAM